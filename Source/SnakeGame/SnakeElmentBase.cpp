// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElmentBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeElmentBase::ASnakeElmentBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ASnakeElmentBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeElmentBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void ASnakeElmentBase::SetFirstElementType_Implementation()
{

}

